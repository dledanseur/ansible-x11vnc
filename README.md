# ansible-x11vnc
This modules installs x11vnc and optionnaly creates a startup desktop entry in order for someone to be able to connect to the main user session.

Usage:

```
#!yaml

- hosts:
  roles:
    - dledanseur.x11vnc
  vars:
    x11vnc_autostart_user: auser #optional, defaults to none
    x11vnc_password: password
 
```
